<?php
/**
 * @license All Rights Reserved
 * @author Phuong LE <phuong.le@menincode.com>
 * @copyright Copyright (c) 2021 Men In Code Ltd (https://www.menincode.com)
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Easygento_Core', __DIR__);
